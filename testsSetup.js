import Adapter from 'enzyme-adapter-react-16'
import Enzyme from 'enzyme'
import axios from 'axios'
import httpAdapter from 'axios/lib/adapters/http'

Enzyme.configure({
  adapter: new Adapter()
})

global.host = 'http://localhost'

axios.defaults.host = global.host
axios.defaults.adapter = httpAdapter

global.createTest = (payload, action, methods, withCount) => {
  describe(action.name, () => {
    it('REQUEST', () => {
      expect(methods[0]()).toEqual({ type: action.request })
    })
    it('SUCCESS', () => {
      expect(methods[1](payload, 1)).toEqual({
        type: action.success,
        payload: withCount
          ? {
            data: payload,
            count: 1
          }
          : payload
      })
    })
    it('FAILURE', () => {
      expect(methods[2]()).toEqual({ type: action.failure })
    })
  })
}
