export const getItemsByKeys = (keys = [], items = {}) => {
  if (
    keys === null ||
    items === null ||
    !(keys instanceof Array) ||
    !(items instanceof Object) ||
    !keys.length
  ) {
    return []
  }

  return keys.reduce((result, id) => {
    return items[id]
      ? [ ...result, items[id] ]
      : result
  }, [])
}

const defaultGetter = item => item.id

export const normalizeList = (list = [], getParam = defaultGetter) => {
  const defaultObject = {
    keys: [],
    objects: {}
  }
  if (
    list === null ||
    !(list instanceof Array) ||
    !list.length ||
    typeof getParam !== 'function'
  ) {
    return defaultObject
  }
  return list.reduce((result, item) => {
    const param = getParam(item)
    return param
      ? {
        keys: [ ...result.keys, param ],
        objects: {
          ...result.objects,
          [param]: item
        }
      }
      : result
  }, defaultObject)
}

export const objectFromKeys = (keys = [], defaultValue = {}) => {
  if (
    !Array.isArray(keys) ||
    !keys.length
  ) {
    return {}
  }

  return keys.reduce((result, item) => ({
    ...result,
    [item]: defaultValue
  }), {})
}

export const convertArrayToObject = (
  list = [],
  getKey = i => i.id,
  getValue = i => i,
  defaultValue = ''
) => {
  if (
    !Array.isArray(list) ||
    !list.length ||
    typeof getKey !== 'function' ||
    typeof getValue !== 'function'
  ) {
    return {}
  }
  return list.reduce((result, item) => ({
    ...result,
    [getKey(item)]: getValue(item) || defaultValue
  }), {})
}
