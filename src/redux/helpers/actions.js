import { actionSuffixes } from "../middlewares/loaderMiddleware"

export const createRequestAction = name => ({
  request: `${name}_${actionSuffixes.request}`,
  success: `${name}_${actionSuffixes.success}`,
  failure: `${name}_${actionSuffixes.failure}`,
  name
})
