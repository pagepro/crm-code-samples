class StateManager {
  constructor (store) {
    const {
      dispatch,
      getState
    } = store

    Object.assign(this, {
      dispatch,
      getState
    })
  }

  static initialize (store) {
    if (!store || typeof store !== 'object') throw new TypeError()

    if (!StateManager.instance) {
      StateManager.instance = new StateManager(store)
    }
  }

  static getInstance () {
    return StateManager.instance
  }
}

const initialize = StateManager.initialize
const instance = StateManager.getInstance

export {
  instance as default,
  initialize
}
