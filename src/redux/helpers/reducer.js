export const checkTableItemPayload = (payload, state) => (
  payload.id &&
  state[payload.id] &&
  payload.item &&
  (
    typeof payload.item === 'string' ||
    typeof payload.item === 'number'
  )
)

export const checkTablePayload = (payload, state) => (
  payload.id &&
  state[payload.id]
)

export const checkTableInitParams = payload => {
  return payload.params && Object.keys(payload.params).length
}
