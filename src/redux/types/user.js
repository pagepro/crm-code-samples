import { createRequestAction } from '../helpers/actions'

/* user */
export const RESET_USER_ACTION = 'RESET_USER_ACTION'
export const CREATE_USER = createRequestAction('CREATE_USER')
export const UPDATE_USER = createRequestAction('UPDATE_USER')
export const GET_USERS = createRequestAction('GET_USERS')
export const GET_USER_PERMISSIONS = createRequestAction('GET_USER_PERMISSIONS')
export const ADD_USER_PERMISSIONS = createRequestAction('ADD_USER_PERMISSIONS')
export const REMOVE_USER_PERMISSIONS = createRequestAction('REMOVE_USER_PERMISSIONS')
export const DISABLE_USER = createRequestAction('DISABLE_USER')
export const ENABLE_USER = createRequestAction('ENABLE_USER')
