/**
 * Action types suffixes for each action that should run asynchronously
 * @type {{request: string, success: string, failure: string, start: string, end: string}}
 */
export const actionSuffixes = {
  request: 'REQUEST',
  success: 'SUCCESS',
  failure: 'FAILURE',
  start: 'START',
  end: 'END'
}

/**
 * Loader action types creator
 * @param {string} name
 * @returns {{start: string, end: string}}
 */
const createStartEndAction = name => ({
  start: `${name}_${actionSuffixes.start}`,
  end: `${name}_${actionSuffixes.end}`
})

/**
 * Loader action types
 * @type {{start: string, end: string}}
 */
export const LOADER = createStartEndAction('LOADER')

/**
 * Loader middleware. It puts all actions dispatched during API request on stack.
 * Before first action, that type ends with 'REQUEST', middleware dispatch action LOADER_START.
 * All actions are hold on stock.
 * When dispatched actions with suffix 'SUCCESS' or 'FAILURE' equals dispatched 'REQUEST',
 * then middleware dispatch action with type LOADER_END
 */
export default () => {
  let requests = []
  let finished = []
  return next => {
    return action => {
      const isSuccessAction = action.type.endsWith(`_${actionSuffixes.success}`)
      const isRequestAction = action.type.endsWith(`_${actionSuffixes.request}`)
      const isFailureAction = action.type.endsWith(`_${actionSuffixes.failure}`)

      if (!isFailureAction && !isSuccessAction && !isRequestAction) {
        return next(action)
      }

      if (isRequestAction) {
        if (!requests.length) {
          next({
            type: LOADER.start
          })
        }
        requests.push(action.type)
        return next(action)
      } else {
        finished.push(action)
        next(action)
      }
      if (requests.length && finished.length && finished.length === requests.length) {
        requests = []
        finished = []
        return next({
          type: LOADER.end
        })
      }
    }
  }
}
