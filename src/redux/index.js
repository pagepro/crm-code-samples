import {
  applyMiddleware,
  compose,
  createStore
} from 'redux'
import thunk from 'redux-thunk'
import loaderMiddleware from './middlewares/loaderMiddleware'
import AppReducer from './reducers'

const devTools = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__()
  : a => a

const store = createStore(
  AppReducer,
  compose(
    applyMiddleware(
      thunk,
      loaderMiddleware
    ),
    devTools
  )
)

export default store
