import {
  tableInitAction,
  tableDestroyAction,
  changeTableParamsAction,
  selectItemAction,
  deselectItemAction,
  deselectAllItemsAction,
  tableFilterAction,
  resetTableParamsAction
} from './ActionCreators'

export const initTable = (id, data = {}) => dispatch => {
  dispatch(tableInitAction(id, data))
}

export const destroyTable = id => dispatch => {
  dispatch(tableDestroyAction(id))
}

export const changeTableParams = (id, params = {}) => dispatch => {
  dispatch(changeTableParamsAction(id, params))
}

export const resetTableParams = (id) => dispatch => {
  dispatch(resetTableParamsAction(id))
}

export const selectItem = (id, item) => dispatch => {
  dispatch(selectItemAction(id, item))
}

export const deselectItem = (id, item) => dispatch => {
  dispatch(deselectItemAction(id, item))
}

export const deselectAllItems = id => dispatch => {
  dispatch(deselectAllItemsAction(id))
}

export const filterItems = (id, filters) => dispatch => {
  dispatch(tableFilterAction(id, filters))
}
