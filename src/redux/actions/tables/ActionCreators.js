import * as actionTypes from '../../types/table'

export const tableInitAction = (id, params) => ({
  type: actionTypes.TABLE_INIT,
  payload: {
    id,
    params
  }
})

export const tableDestroyAction = id => ({
  type: actionTypes.TABLE_DESTROY,
  payload: id
})

export const changeTableParamsAction = (id, params) => ({
  type: actionTypes.TABLE_UPDATE_PARAMS,
  payload: {
    id,
    params
  }
})

export const resetTableParamsAction = id => ({
  type: actionTypes.TABLE_RESET_PARAMS,
  payload: {
    id
  }
})

export const selectItemAction = (id, item) => ({
  type: actionTypes.TABLE_SELECT_ITEM,
  payload: {
    id,
    item
  }
})

export const deselectItemAction = (id, item) => ({
  type: actionTypes.TABLE_DESELECT_ITEM,
  payload: {
    id,
    item
  }
})

export const deselectAllItemsAction = id => ({
  type: actionTypes.TABLE_DESELECT_ALL_ITEMS,
  payload: id
})

export const tableFilterAction = (id, data = {filters: {}, autocomplete: {}}) => ({
  type: actionTypes.TABLE_FILTER,
  payload: {
    id,
    ...data
  }
})
