import {
  CREATE_USER,
  UPDATE_USER,
  GET_USERS,
  RESET_USER_ACTION,
  ADD_USER_PERMISSIONS,
  REMOVE_USER_PERMISSIONS,
  DISABLE_USER,
  ENABLE_USER,
  GET_USER_PERMISSIONS
} from '../../types/user'

/* GET */
export const getUserRequest = () => ({
  type: GET_USERS.request
})

export const getUserSuccess = (data, count = 0) => ({
  type: GET_USERS.success,
  payload: {
    data,
    count
  }
})

export const getUserFailure = () => ({
  type: GET_USERS.failure
})

/* ADD */
export const createUserRequest = () => ({
  type: CREATE_USER.request
})

export const createUserSuccess = payload => ({
  type: CREATE_USER.success,
  payload
})

export const createUserFailure = payload => ({
  type: CREATE_USER.failure,
  payload
})

/* EDIT */
export const updateUserRequest = () => ({
  type: UPDATE_USER.request
})

export const updateUserSuccess = payload => ({
  type: UPDATE_USER.success,
  payload
})

export const updateUserFailure = () => ({
  type: UPDATE_USER.failure
})

/* RESET */
export const resetUserAction = () => ({
  type: RESET_USER_ACTION
})

/* GET PERMISSION */
export const getUserPermissionsRequest = () => ({
  type: GET_USER_PERMISSIONS.request
})

export const getUserPermissionsSuccess = (id, data) => ({
  type: GET_USER_PERMISSIONS.success,
  payload: {
    id,
    data
  }
})

export const getUserPermissionsFailure = error => ({
  type: GET_USER_PERMISSIONS.failure,
  error
})

/* ADD PERMISSION */
export const addUserPermissionsRequest = () => ({
  type: ADD_USER_PERMISSIONS.request
})

export const addUserPermissionsSuccess = (id, permissions) => ({
  type: ADD_USER_PERMISSIONS.success,
  payload: {
    id,
    data: permissions
  }
})

export const addUserPermissionsFailure = () => ({
  type: ADD_USER_PERMISSIONS.failure
})

/* DISABLE USER */
export const disableUserRequest = () => ({
  type: DISABLE_USER.request
})

export const disableUserSuccess = id => ({
  type: DISABLE_USER.success,
  payload: {
    id
  }
})

export const disableUserFailure = () => ({
  type: DISABLE_USER.failure
})

/* ENABLE USER */
export const enableUserRequest = () => ({
  type: ENABLE_USER.request
})

export const enableUserSuccess = id => ({
  type: ENABLE_USER.success,
  payload: {
    id
  }
})

export const enableUserFailure = () => ({
  type: ENABLE_USER.failure
})

/* REMOVE PERMISSION */
export const removeUserPermissionsRequest = () => ({
  type: REMOVE_USER_PERMISSIONS.request
})

export const removeUserPermissionsSuccess = (id, permissions) => ({
  type: REMOVE_USER_PERMISSIONS.success,
  payload: {
    id,
    data: permissions
  }
})

export const removeUserPermissionsFailure = () => ({
  type: REMOVE_USER_PERMISSIONS.failure
})
