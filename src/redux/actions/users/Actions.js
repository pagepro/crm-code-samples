import UserService from '../../../api/services/UserService'
import {
  getUserRequest,
  getUserSuccess,
  getUserFailure,
  createUserRequest,
  createUserSuccess,
  createUserFailure,
  updateUserRequest,
  updateUserSuccess,
  updateUserFailure,
  resetUserAction,
  addUserPermissionsRequest,
  addUserPermissionsSuccess,
  addUserPermissionsFailure,
  removeUserPermissionsRequest,
  removeUserPermissionsSuccess,
  removeUserPermissionsFailure,
  disableUserFailure,
  disableUserRequest,
  disableUserSuccess,
  enableUserRequest,
  enableUserSuccess,
  enableUserFailure,
  getUserPermissionsRequest,
  getUserPermissionsSuccess,
  getUserPermissionsFailure
} from './ActionCreators'

const createService = () => new UserService()

export const getUsers = params => dispatch => {
  const service = createService()
  dispatch(getUserRequest())
  return service.get({ params })
    .then(data => {
      if (data && data.items) {
        dispatch(getUserSuccess(data.items, data.meta.max_number))
      }
    })
    .catch(() => {
      dispatch(getUserFailure())
    })
}

export const createUser = user => dispatch => {
  dispatch(createUserRequest())
  return createService().create(user, true)
    .then(data => {
      dispatch(createUserSuccess(data))
    })
    .catch(error => {
      dispatch(createUserFailure())
    })
}

export const updateUser = user => dispatch => {
  dispatch(updateUserRequest())
  return createService().update(user, true)
    .then(user => {
      dispatch(updateUserSuccess(user))
    })
    .catch(() => {
      dispatch(updateUserFailure())
    })
}

export const getUserDetails = id => dispatch => {
  dispatch(getUserRequest())
  return createService().getById(id)
    .then(user => {
      dispatch(getUserSuccess([ user ]))
    })
    .catch(() => {
      dispatch(getUserFailure())
    })
}

export const resetAction = () => dispatch => {
  return dispatch(resetUserAction())
}

export const addUserPermissions = (id, permissions) => dispatch => {
  if (!(Array.isArray(permissions))) return Promise.resolve()
  dispatch(addUserPermissionsRequest())
  return createService().addPermissions(id, permissions)
    .then(() => {
      dispatch(addUserPermissionsSuccess(id, permissions))
    })
    .catch(() => {
      dispatch(addUserPermissionsFailure())
    })
}

export const removeUserPermissions = (id, permissions) => dispatch => {
  if (!(Array.isArray(permissions))) return Promise.resolve()
  dispatch(removeUserPermissionsRequest())
  return createService().removePermissions(id, permissions)
    .then(() => {
      dispatch(removeUserPermissionsSuccess(id, permissions))
    })
    .catch(() => {
      dispatch(removeUserPermissionsFailure())
    })
}

export const disableUser = id => dispatch => {
  dispatch(disableUserRequest())
  return createService().disableUser(id)
    .then(() => {
      dispatch(disableUserSuccess(id))
    })
    .catch(() => {
      dispatch(disableUserFailure())
    })
}

export const getUserPermissions = id => dispatch => {
  dispatch(getUserPermissionsRequest())
  return createService().getPermissions(id)
    .then(permissions => {
      dispatch(getUserPermissionsSuccess(id, permissions))
    })
    .catch(() => {
      dispatch(getUserPermissionsFailure())
    })
}

export const enableUser = id => dispatch => {
  dispatch(enableUserRequest())
  return createService().enableUser(id)
    .then(() => {
      dispatch(enableUserSuccess(id))
    })
    .catch(() => {
      dispatch(enableUserFailure())
    })
}
