import { combineReducers } from 'redux'
import usersReducer from './users'
import tableReducer from './table'

/**
 * All reducers goes here, combined into AppReducer
 */
const reducer = combineReducers({
  usersModule: usersReducer,
  table: tableReducer
})

export default reducer
