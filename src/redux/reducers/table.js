import * as actionTypes from '../types/table'

import {
  checkTablePayload,
  checkTableItemPayload,
  checkTableInitParams
} from '../helpers/reducer'

export const initialState = {}
export const tableInitParams = {
  page: 1,
  limit: 10,
  selected: {},
  filters: {},
  autocomplete: {},
  sortBy: {}
}

export default (state = initialState, action) => {
  const {
    type,
    payload
  } = action

  switch (type) {
    case actionTypes.TABLE_INIT:
      const initialParams = checkTableInitParams(payload)
        ? payload.params
        : tableInitParams

      if (!payload.id) {
        return state
      }

      return {
        ...state,
        [payload.id]: {
          id: payload.id,
          ...initialParams
        }
      }

    case actionTypes.TABLE_DESTROY:
      if (!payload) {
        return state
      }
      const {
        [payload]: _,
        ...rest
      } = state

      return rest

    case actionTypes.TABLE_UPDATE_PARAMS:
      if (!checkTablePayload(payload, state)) {
        return state
      }

      return {
        ...state,
        [payload.id]: {
          ...state[payload.id],
          ...payload.params
        }
      }

    case actionTypes.TABLE_RESET_PARAMS:
      if (!payload.id) {
        return state
      }

      return {
        ...state,
        [payload.id]: {
          ...tableInitParams
        }
      }

    case actionTypes.TABLE_SELECT_ITEM:
      if (!checkTableItemPayload(payload, state)) {
        return state
      }

      return {
        ...state,
        [payload.id]: {
          ...state[payload.id],
          selected: {
            ...state[payload.id].selected,
            [payload.item]: true
          }
        }
      }

    case actionTypes.TABLE_DESELECT_ITEM:
      if (!checkTableItemPayload(payload, state)) {
        return state
      }

      const {
        [payload.item.toString()]: __, // eslint-disable-line
        ...updatedSelectedItems
      } = state[payload.id].selected

      return {
        ...state,
        [payload.id]: {
          ...state[payload.id],
          selected: updatedSelectedItems
        }
      }

    case actionTypes.TABLE_DESELECT_ALL_ITEMS:
      if (!checkTablePayload({ id: payload }, state)) {
        return state
      }

      return {
        ...state,
        [payload]: {
          ...state[payload],
          selected: {}
        }
      }

    case actionTypes.TABLE_FILTER:
      if (!checkTablePayload(payload, state)) {
        return state
      }

      return {
        ...state,
        [payload.id]: {
          ...state[payload.id],
          filters: payload.filters,
          autocomplete: payload.autocomplete
        }
      }
    default:
      return state
  }
}
