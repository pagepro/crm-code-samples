import {
  CREATE_USER,
  UPDATE_USER,
  GET_USERS,
  RESET_USER_ACTION,
  DISABLE_USER,
  ENABLE_USER,
  GET_USER_PERMISSIONS
} from '../types/user'
import {
  normalizeList
} from '../helpers/selectors'

export const initialState = {
  users: {},
  usersList: [],
  actionFulfilled: false,
  isDataFetched: false,
  count: 0,
  permissions: {}
}

export default (state = initialState, action) => {
  const {
    type,
    payload
  } = action
  switch (type) {
    case CREATE_USER.success:
      const isAlreadyOnList = ~state.usersList.indexOf(payload.id)

      return {
        ...state,
        actionFulfilled: true,
        usersList: isAlreadyOnList
          ? state.usersList
          : [
            ...state.usersList,
            payload.id
          ],
        users: {
          ...state.users,
          [payload.id]: isAlreadyOnList
            ? {
              ...state.users[payload.id],
              ...payload
            }
            : payload
        },
        permissions: {
          ...state.permissions,
          [payload.id]: []
        }
      }

    case UPDATE_USER.success:
      return {
        ...state,
        actionFulfilled: true,
        users: {
          ...state.users,
          [payload.id]: payload
        }
      }

    case GET_USERS.success:
      const normalizedUsers = normalizeList(payload.data)

      return {
        ...state,
        users: normalizedUsers.objects,
        usersList: normalizedUsers.keys,
        isDataFetched: true,
        count: payload.count
      }

    case RESET_USER_ACTION:
      return {
        ...state,
        actionFulfilled: false
      }

    case DISABLE_USER.success:
      return {
        ...state,
        users: {
          [payload.id]: {
            ...state.users[payload.id],
            enabled: false
          }
        }
      }

    case ENABLE_USER.success:
      return {
        ...state,
        users: {
          [payload.id]: {
            ...state.users[payload.id],
            enabled: true
          }
        }
      }

    case GET_USER_PERMISSIONS.success:
      return {
        ...state,
        permissions: {
          ...state.permissions,
          ...payload
        }
      }

    default:
      return state
  }
}
