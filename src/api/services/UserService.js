import ApiService from '../ApiService'

/**
 * API endpoints configuration sample
 */
const userBase = 'user'
const user = {
  base: id => `${userBase}/user${id ? `/${id}` : ''}`,
  disable: id => `${userBase}/user/${id}/disable`,
  enable: id => `${userBase}/user/${id}/enable`,
  permission: id => `${userBase}/user/${id}/permissions`
}

/**
 * Api service implementation according to API entity namespace
 * UserService inherits common CRUD calls from ApiService class
 * Any custom API calls can be declared here
 */
class UsersService extends ApiService {
  constructor () {
    super(user)
  }

  getPermissions (id) {
    return this.service().get(this.routes.permission(id))
  }

  addPermissions (id, permissions) {
    return this.service().post(this.routes.permission(id), {
      permissions,
      force_creation: true
    })
  }

  removePermissions (id, permissions) {
    return this.service().delete(this.routes.permission(id), { permissions })
  }

  disableUser (id) {
    return this.service().post(this.routes.disable(id))
  }

  enableUser (id) {
    return this.service().post(this.routes.enable(id))
  }
}

export default UsersService
