export class RequestInterceptor {
  resolve (config) {
    return Promise.resolve(config)
  }
}

export class ResponseInterceptor {
  resolve (response) {
    return Promise.resolve(response)
  }

  reject (error) {
    return Promise.reject(error)
  }
}
