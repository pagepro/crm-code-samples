import axios from 'axios'
import PushErrorNotification from './response/pushErrorNotification'

const requestInterceptors = []

const responseInterceptors = [
  new PushErrorNotification()
]

const registerInterceptors = (axiosInstance = axios) => {
  requestInterceptors.forEach(({ resolve }) => {
    axiosInstance.interceptors.request.use(resolve)
  })

  responseInterceptors.forEach(({ resolve, reject }) => {
    axiosInstance.interceptors.response.use(resolve, reject)
  })
}

export {
  registerInterceptors as default
}
