import { ResponseInterceptor } from '../interceptor'
import StateManager from '../../../redux/helpers/StateManager'
import { pushNotification } from 'common/notifications/notificationsAction'

export default class PushErrorNotification extends ResponseInterceptor {
  reject (error) {
    if (error.response.status === 500) {
      const stateManagerInstance = StateManager.getInstance()

      stateManagerInstance
        .dispatch(pushNotification({
          type: 'danger',
          headline: 'Error 500',
          message: 'Internal server error'
        }))
    }

    return Promise.reject(error)
  }
}
