import Axios from './Axios'
import { throwFormErrors } from 'helpers/services'

const routesShape = {
  base: () => ''
}
const routesConfig = Object.create(routesShape)

class ApiService {
  constructor (routes) {
    this.apiFormService = new Axios({ onError: throwFormErrors })
    this.apiService = new Axios()

    this.routes = routes || routesShape
  }

  service (isForm) {
    return isForm
      ? this.apiFormService
      : this.apiService
  }

  get (params, url = this.routes.base()) {
    return this.service().get(url, params)
  }

  getById (id) {
    return this.service().get(this.routes.base(id))
  }

  create (data, isForm) {
    return this.service(isForm).post(this.routes.base(), data)
  }

  update ({id, ...data}, isForm) {
    return this.service(isForm).put(this.routes.base(id), data)
  }

  getMultipleItems (params, url = this.routes.base()) {
    return this.get(params, url).then(({ items, meta: { page, limit, max_number: itemsCount } }) => {
      const resultItems = [...items]
      const pages = Array.range(
        page + 1,
        Math.round(itemsCount / limit)
      )

      return Promise
        .all(pages.map(page => {
          return this.get({ params: { page } })
            .then(data => resultItems.push(...data.items))
        }))
        .then(() => [resultItems, itemsCount])
    })
  }
}

export {
  ApiService as default,
  routesConfig
}
