import snakeCase from 'lodash/snakeCase'

export const extractData = response => {
  return response && response.data
}

export const extractError = error => {
  return error && error.response && error.response.data
}

export const getFormErrors = error => {
  if (error && error instanceof Array && error.length) {
    return error.reduce((result, item) => ({
      ...result,
      [snakeCase(item.property_path)]: item.message
    }), { _error: 'Error' })
  }
  return { _error: 'Error' }
}

export const throwFormErrors = response => {
  throw getFormErrors(extractError(response))
}
