import axios from 'axios'
import {
  extractData,
  extractError
} from './helpers/services'
import registerInterceptors from './interceptors'

const types = {
  GET: 'get',
  POST: 'post',
  PUT: 'put',
  DELETE: 'delete'
}

class Axios {
  constructor ({ onSuccess, onError } = {}) {
    this.axios = null
    this.onSuccess = onSuccess || extractData
    this.onError = onError || extractError
  }

  /**
   * Proxy for create new instance every time.
   */
  _initializeAxios () {
    if (this.axios === null) {
      this.axios = axios.create(Axios.globalConfig)
      registerInterceptors(this.axios)
    }
  }

  request ({ type, url = '', data = {}, options = {} }) {
    this._initializeAxios()
    let fetcher = null
    if (type === types.GET) {
      fetcher = this.axios.get(url, options)
    } else if (type === types.DELETE) {
      fetcher = this.axios.delete(url, {
        ...options,
        data
      })
    } else {
      fetcher = this.axios[type](url, data, options)
    }

    return fetcher
      .then(response => {
        return this.onSuccess(response)
      })
      .catch(error => {
        throw this.onError(error)
      })
  }

  getMultiple (urls) {
    const promises = urls.map(item => axios.get(item))
    return Promise.all(promises)
      .then(data => {
        return data.map(extractData)
      })
  }

  get (url, options) {
    return this.request({
      type: types.GET,
      url,
      options
    })
  }

  put (url, data, options) {
    return this.request({
      type: types.PUT,
      url,
      data,
      options
    })
  }

  post (url, data, options) {
    return this.request({
      type: types.POST,
      url,
      data,
      options
    })
  }

  delete (url, data, options) {
    return this.request({
      type: types.DELETE,
      url,
      data,
      options
    })
  }
}

/**
 * Axios config used when creating new axios instance.
 * It can be modified by Api Services to use headers, etc.
 */
Axios.globalConfig = {
  baseURL: `${process.env.REACT_APP_API_URL}/api`,
  headers: {}
}

export {
  Axios as default,
  types as requestTypes
}
