export const SMALL = 'SMALL'
export const NORMAL = 'NORMAL'
export const MEDIUM = 'MEDIUM'
export const BIG = 'BIG'

const valueToPixels = value => `${value}px`

export const FONT_SIZES = {
  [SMALL]: valueToPixels(12),
  [NORMAL]: valueToPixels(13),
  [MEDIUM]: valueToPixels(16),
  [BIG]: valueToPixels(20)
}

export const COLORS = {
  BLACK: '#000000',
  WHITE: '#ffffff',
  GOLD: '#c09a57',
  DARKEN_GOLD: '#b09153',
  GREY: '#979797',
  LIGHTER_GREY: '#d1d1d1',
  DARK_GREY: '#2a2a2a',
  LIGHT_GREY_ACTION: '#d8d8d85e',
  GREYICONS: '#cfd7de',
  LIGHTBLUE: '#dae4ed',
  RED: '#f22323',
  SHADOW: '#0000001c',
  DARK_THEME_BORDER: '#ffffff12',
  DARK_THEME_BACKGROUND: '#0000001a',
  DARK_BLUE: '#232e38',
  GREEN: '#009900'
}
