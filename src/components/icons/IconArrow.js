import styled from 'styled-components'
import { COLORS } from '../consts'

export const ARROW_DIRECTIONS = {
  UP: 'up',
  DOWN: 'down',
  LEFT: 'left',
  RIGHT: 'right'
}

Object.freeze(ARROW_DIRECTIONS)

const arrowRotation = {
  up: '-135deg',
  down: '45deg',
  left: '135deg',
  right: '-45deg'
}

const BaseArrow = styled.span`
  border: solid ${({ inactive }) => inactive ? COLORS.GREYICONS : COLORS.GOLD};
  border-width: 0 2px 2px 0;
  display: inline-block;
  padding: 2px;
`

const IconArrow = BaseArrow.extend`
  transform: rotate(${({ direction }) => arrowRotation[direction || ARROW_DIRECTIONS.DOWN]});
`

export const IconArrowDown = BaseArrow.extend`
  transform: rotate(${arrowRotation[ARROW_DIRECTIONS.DOWN]});
`

export const IconArrowUp = BaseArrow.extend`
  transform: rotate(${arrowRotation[ARROW_DIRECTIONS.UP]});
`

export const IconArrowLeft = BaseArrow.extend`
  transform: rotate(${arrowRotation[ARROW_DIRECTIONS.LEFT]});
`

export const IconArrowRight = BaseArrow.extend`
  transform: rotate(${arrowRotation[ARROW_DIRECTIONS.RIGHT]});
`

export default IconArrow
