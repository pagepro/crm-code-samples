import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { COLORS, FONT_SIZES } from '../consts'
import ShowWhen from '../../common/ShowWhen'
import DoubleIconBox from './DoubleIconBox'

const TableHeader = styled.a`
  width: ${props => props.width || '100%'};
  min-width: 0;

  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-grow: 1;

  overflow: hidden;

  font-weight: 700;
  font-size: ${FONT_SIZES.SMALL};
  color: rgba(${COLORS.BLACK}, ${props => props.fade ? '0.4' : '1'});
  letter-spacing: 0.1px;
  white-space: nowrap;
  text-overflow: ellipsis;

  padding: 0 23px;

  cursor: ${props => props.clickable ? 'pointer' : 'auto'};
  user-select: none;
  > span {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    font-weight: 700;
  }
`

TableHeader.propTypes = {
  /** Sets 0.4 opacity for header */
  fade: PropTypes.bool,
  clickable: PropTypes.bool,
  width: PropTypes.oneOfType([
    PropTypes.string
  ])
}

export const TableHeaderWithArrows = ({ direction, active, ...props }) => {
  return (
    <TableHeader {...props}>
      <span>
        {props.children}
      </span>
      <ShowWhen when={active}>
        <DoubleIconBox
          direction={direction}
        />
      </ShowWhen>
    </TableHeader>
  )
}

TableHeaderWithArrows.propTypes = {
  children: PropTypes.node,
  direction: PropTypes.string,
  active: PropTypes.bool
}

TableHeader.defaultValues = {
  fade: false,
  clickable: false
}

export default TableHeader
