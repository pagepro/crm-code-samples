import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { IconArrowDown, IconArrowUp } from '../icons/IconArrow'
import { ORDER } from '../../table/BaseTableHeader'

const Box = styled.span`
  display: inline-flex;
  height: 100%;
  flex-flow: column nowrap;
  width: 6px;

  justify-content: center;
`

const DoubleIconBox = ({ direction }) => {
  return (
    <Box>
      <IconArrowUp
        inactive={ORDER.ASCENDING !== direction}
      />
      <IconArrowDown
        inactive={ORDER.DESCENDING !== direction}
      />
    </Box>
  )
}

DoubleIconBox.propTypes = {
  direction: PropTypes.string
}

export default DoubleIconBox
