import styled from 'styled-components'

const TableRowHeader = styled.div`
  height: 19px;
  margin-bottom: 4px;

  display: flex;
  flex-flow: row nowrap;
`

export default TableRowHeader
