import React from 'react'
import PropTypes from 'prop-types'

const ShowWhen = ({ when, children, onRenderMountOnly = false }) => {
  const wrappedChildren = (
    <div style={{ display: when ? 'initial' : 'none' }}>
      { children }
    </div>
  )
  return when
    ? children
    : onRenderMountOnly
      ? wrappedChildren
      : null
}

ShowWhen.propTypes = {
  when: PropTypes.bool,
  children: PropTypes.node,
  onRenderMountOnly: PropTypes.bool
}

export default ShowWhen
