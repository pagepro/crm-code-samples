import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { filterItems } from '../redux/actions/tables/Actions'
import debounce from 'lodash/debounce'
import validate from './validate'

export class TableQueries extends Component {
  constructor (props) {
    super(props)

    this.filterData = this.filterData.bind(this)
    this.onAutocomplete = this.onAutocomplete.bind(this)
    this.onChange = this.onChange.bind(this)
    this.fetchMethod = debounce(this.fetchMethod.bind(this), 1000)
  }

  fetchMethod (query) {
    this.props.fetchMethod(query)
  }

  filterData (params, suggestion = {}) {
    const {
      filters,
      persistentParams,
      autocomplete,
      table,
      filterItems
    } = this.props
    filterItems(table, {
      filters: {
        ...filters,
        ...params
      },
      autocomplete: {
        ...autocomplete,
        ...suggestion
      }
    })
    this.fetchMethod({
      ...persistentParams,
      ...filters,
      ...params
    })
  }

  onChange (e) {
    const {
      name,
      value
    } = e.target
    const params = {
      [name]: value
    }
    this.filterData(params)
    e.preventDefault()
  }

  onAutocomplete (params, autocomplete) {
    this.filterData(params, autocomplete)
  }

  render () {
    const {
      component: Component,
      ...props
    } = this.props
    const tableProps = {
      handleFilter: this.onChange,
      handleFilterAutocomplete: this.onAutocomplete
    }

    return (
      <Component
        {...props}
        {...tableProps}
      />
    )
  }
}

TableQueries.propTypes = {
  filters: PropTypes.object,
  autocomplete: PropTypes.object,
  table: PropTypes.string,
  filtersConfig: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    fetchMethod: PropTypes.func,
    suggestionValue: PropTypes.func
  })),
  fetchMethod: PropTypes.func.isRequired,
  filterItems: PropTypes.func.isRequired,
  component: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.func
  ]).isRequired
}

const tableQueries = ({
  fetchMethod = () => Promise.resolve(),
  table
}, filtersConfig = []) => component => {
  validate([ fetchMethod ], [ table ])

  const mapStateToProps = state => {
    const tableData = state.table[table] || {}

    return {
      filters: tableData.filters || {},
      autocomplete: tableData.autocomplete || {},
      persistentParams: tableData.persistentParams || {}
    }
  }

  return connect(mapStateToProps, {
    fetchMethod,
    filterItems
  })(props =>
    <TableQueries
      table={table}
      component={component}
      filtersConfig={filtersConfig}
      {...props}
    />)
}

export default tableQueries
