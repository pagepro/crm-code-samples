import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import {
  destroyTable,
  initTable,
  changeTableParams,
  resetTableParams,
  deselectAllItems,
  deselectItem,
  selectItem
} from '../redux/actions/tables/Actions'
import BaseTableHeader from './BaseTableHeader'
import validate from './validate'

export class TableWrapper extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      isLoading: true,
      isMounted: false
    }

    this.getData = this.getData.bind(this)
    this.destroyTable = this.destroyTable.bind(this)
    this.changePage = this.changePage.bind(this)
    this.changeSort = this.changeSort.bind(this)
    this.resetSort = this.resetSort.bind(this)
    this.initTable = this.initTable.bind(this)
    this.toggleLoader = this.toggleLoader.bind(this)
    this.deselectAll = this.deselectAll.bind(this)
    this.selectItem = this.selectItem.bind(this)
    this.deselectItem = this.deselectItem.bind(this)
  }

  initTable () {
    const {
      table,
      page,
      limit,
      filters,
      initTable,
      sortBy,
      selected,
      initialQuery
    } = this.props
    initTable(table, {
      page,
      limit,
      filters,
      sortBy,
      selected,
      persistentParams: initialQuery
    })

    this.setState({ isMounted: true })
  }

  deselectAll () {
    this.props.deselectAllItems(this.props.table)
  }

  deselectItem (rowData) {
    this.props.deselectItem(this.props.table, this.props.selectItemSelector(rowData))
  }

  selectItem (rowData) {
    if (this.props.singleItemTable) {
      this.deselectAll()
    }
    this.props.selectItem(this.props.table, this.props.selectItemSelector(rowData))
  }

  toggleLoader (isLoading = false) {
    this.setState({ isLoading })
  }

  componentDidMount () {
    this.initTable()
    this.getData(this.props.initialQuery)
  }

  componentWillUnmount () {
    this.destroyTable()
  }

  getData (params = {}) {
    const {
      page,
      limit,
      sortBy
    } = this.props

    const {
      paramPage,
      paramLimit,
      paramSort,
      paramDirection,
      ...otherParams
    } = params

    this.toggleLoader(true)

    return this.props.fetchMethod({
      page: paramPage || page,
      limit: paramLimit || limit,
      sort: paramSort || sortBy.sort,
      direction: paramDirection || sortBy.direction,
      ...otherParams
    })
      .then(() => {
        this.toggleLoader()
        !this.props.cached.length && this.deselectAll()
      })
  }

  destroyTable () {
    this.props.destroyTable(this.props.table)
  }

  changePage (page) {
    this.props.changeTableParams(this.props.table, { page })
    this.getData({ page })
  }

  changeSort (sortBy) {
    this.props.changeTableParams(this.props.table, { sortBy })
    this.getData({ ...sortBy })
  }

  resetSort () {
    this.props.resetTableParams(this.props.table)
    this.getData({ sortBy: {} })
  }

  renderHeader () {
    const {
      header,
      sortBy
    } = this.props

    return <BaseTableHeader
      columns={header}
      changeSort={this.changeSort}
      sortBy={sortBy}
    />
  }

  render () {
    const {
      deselectAllItems,
      data,
      limit,
      page,
      count,
      selected,
      header,
      sortBy,
      cached,
      initialQuery,
      /* eslint-disable */
      changeTableParams: _,
      destroyTable: __,
      deselectItem: ___,
      initTable: ____,
      fetchMethod: _____,
      selectItem: ______,
      resetTableParams: _______,
      /* eslint-enable */
      component: Component,
      ...normalProps
    } = this.props
    const props = {
      ...normalProps,
      table: {
        data,
        limit,
        page,
        count,
        selected,
        sortBy,
        cached,
        deselectAllItems: this.deselectAll,
        changePage: this.changePage,
        changeSort: this.changeSort,
        getData: this.getData,
        isLoading: this.state.isLoading,
        selectItem: this.selectItem,
        deselectItem: this.deselectItem,
        resetSort: this.resetSort,
        cellSizes: header.map(column => column.width)
      }
    }

    if (!this.state.isMounted) {
      return null
    }

    return header.length
      ? (
        <Fragment>
          {this.renderHeader()}
          <Component {...props} />
        </Fragment>
      )
      : <Component {...props} />
  }
}

TableWrapper.propTypes = {
  fetchMethod: PropTypes.func.isRequired,
  data: PropTypes.array,
  table: PropTypes.string,
  page: PropTypes.number,
  filters: PropTypes.object,
  selectItemSelector: PropTypes.func,
  cached: PropTypes.object,
  count: PropTypes.number,
  limit: PropTypes.number,
  selected: PropTypes.object,
  singleItemTable: PropTypes.bool,
  initTable: PropTypes.func,
  destroyTable: PropTypes.func,
  changeTableParams: PropTypes.func,
  component: PropTypes.func,
  resetTableParams: PropTypes.func,
  deselectAllItems: PropTypes.func,
  deselectItem: PropTypes.func,
  resetSort: PropTypes.func,
  selectItem: PropTypes.func,
  changePage: PropTypes.func,
  changeSort: PropTypes.func,
  initialQuery: PropTypes.object,
  sortBy: PropTypes.shape({
    order: PropTypes.string,
    key: PropTypes.string
  }),
  header: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    slug: PropTypes.string,
    width: PropTypes.string
  }))
}

TableWrapper.defaultProps = {
  data: [],
  page: 1,
  limit: 10,
  sortBy: {},
  filters: {},
  selected: {},
  initialQuery: {},
  cached: []
}

const tableWrapper = (args) => component => {
  const {
    fetchMethod = () => Promise.resolve(),
    dataSelector = state => {},
    countSelector = state => 0,
    cacheSelector = state => [],
    selectItemSelector = item => item,
    singleItemTable = false,
    header = [],
    table
  } = args

  validate([ fetchMethod, dataSelector, countSelector ], [ table ])

  const mapStateToProps = state => {
    const data = dataSelector(state) || []
    const count = countSelector(state) || 0
    const tableData = state.table[table] || {}
    const cached = cacheSelector(state) || []

    return {
      data,
      count,
      page: tableData.page,
      limit: tableData.limit,
      filters: tableData.filters,
      sortBy: tableData.sortBy,
      selected: tableData.selected,
      cached
    }
  }

  return connect(mapStateToProps, {
    destroyTable,
    initTable,
    fetchMethod,
    changeTableParams,
    resetTableParams,
    deselectAllItems,
    deselectItem,
    selectItem
  })(props => (
    <TableWrapper
      table={table}
      header={header}
      selectItemSelector={selectItemSelector}
      dataSelector={dataSelector}
      component={component}
      singleItemTable={singleItemTable}
      {...props}
    />
  ))
}

export default tableWrapper
