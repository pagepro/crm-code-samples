import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import {
  TableHead,
  TableRow,
  SearchTableRow,
  TableGroup,
  TableData
} from 'components/table/styles'
import { TableHeadTitle } from 'components/texts/Texts'
import ShowWhen from 'common/ShowWhen'
import DoubleIconBox from 'components/misc/DoubleIconBox'
import SimpleInput from 'components/inputs/SimpleInput'
import { filterItems } from 'common/table/tableActions'
import {
  connect
} from 'react-redux'
import debounce from 'lodash/debounce'
import {
  dispatchableMethodsPropType
} from 'common/table/tablePropTypes'

export const ORDER = Object.freeze({
  ASCENDING: 'asc',
  DESCENDING: 'desc'
})

class BaseTableHeader extends React.Component {
  constructor () {
    super()

    this.sortBy = this.sortBy.bind(this)
    this.filterData = this.filterData.bind(this)
    this.clearFilter = this.clearFilter.bind(this)
    this.debouncedFilterData = debounce(this.filterData, 500)
    this.renderColumnGroup = this.renderColumnGroup.bind(this)
    this.onFilterInputChange = this.onFilterInputChange.bind(this)
    this.renderHeaderFilters = this.renderHeaderFilters.bind(this)
    this.defaultRenderColumnGroup = this.defaultRenderColumnGroup.bind(this)
    this.defaultRenderHeaderFilter = this.defaultRenderHeaderFilter.bind(this)
  }

  sortBy (slug) {
    return () => {
      if (!slug) return

      const {
        changeSort,
        sortBy: {
          sort,
          direction
        }
      } = this.props

      changeSort({
        sort: slug,
        direction: direction === ORDER.DESCENDING || sort !== slug
          ? ORDER.ASCENDING
          : ORDER.DESCENDING
      })
    }
  }

  defaultRenderColumnGroup (column) {
    const {
      sortBy: {
        direction,
        sort
      }
    } = this.props
    const {
      displayName,
      slug,
      disableSort
    } = column

    return (
      <Fragment>
        <TableHeadTitle onClick={disableSort ? undefined : this.sortBy(slug)}>
          {displayName}
        </TableHeadTitle>
        <ShowWhen when={slug === sort}>
          <DoubleIconBox
            direction={direction}
          />
        </ShowWhen>
      </Fragment>
    )
  }

  renderColumnGroup (group) {
    const {
      groupId,
      groupWidth,
      columns
    } = group

    return (
      <TableGroup
        key={`header-group-${groupId}`}
        width={groupWidth}
      >
        {columns.map(column => {
          const {
            displayName,
            slug,
            width,
            renderers: {
              headerRenderer
            } = Object.empty
          } = column

          return (
            <TableData
              width={width}
              key={`header-${displayName}-${slug}`}
            >
              {
                headerRenderer
                  ? headerRenderer(column)
                  : this.defaultRenderColumnGroup(column)
              }
            </TableData>
          )
        })}
      </TableGroup>
    )
  }

  defaultRenderHeaderFilter (slug) {
    const {
      filters: {
        [slug]: filterValue = String.empty
      },
      isLoading
    } = this.props

    return (
      <SimpleInput
        placeholder={'Szukaj...'}
        type={'text'}
        readOnly={isLoading}
        value={filterValue}
        onChange={this.onFilterInputChange(slug)}
        onClearIconClick={this.clearFilter(slug, true)}
        clearIconShown={!!filterValue}
      />
    )
  }

  renderHeaderFilters (group) {
    const {
      groupId,
      columns,
      groupWidth
    } = group
    const {
      dispatchableMethods,
      filters
    } = this.props

    return (
      <TableGroup
        key={`header-group-${groupId}`}
        width={groupWidth}
      >
        {columns.map(column => {
          const {
            slug,
            displayName,
            width,
            renderers: {
              filterRenderer
            } = Object.empty
          } = column

          return (
            <TableData
              key={`header-filter-${displayName}-${slug}`}
              width={width}
            >
              {
                filterRenderer
                  ? filterRenderer(slug, filters, dispatchableMethods)
                  : this.defaultRenderHeaderFilter(slug)
              }
            </TableData>
          )
        })}
      </TableGroup>
    )
  }

  clearFilter (slug, withFetch = false) {
    return () => {
      const {
        filters,
        table,
        filterItems
      } = this.props

      const {
        [slug]: currentFilterValue,
        ...newFilters
      } = filters

      if (!currentFilterValue) return

      filterItems(table, {
        filters: newFilters
      })

      if (withFetch) {
        this.filterData(newFilters)
      }
    }
  }

  onFilterInputChange (slug) {
    return ({ currentTarget: { value } }) => {
      const {
        filters,
        table,
        filterItems
      } = this.props

      if (!value) {
        this.clearFilter(slug)()
      } else {
        const newFilter = {
          [slug]: value
        }

        filterItems(table, {
          filters: {
            ...filters,
            ...newFilter
          }
        })
      }

      this.debouncedFilterData()
    }
  }

  filterData (filters = this.props.filters) {
    const {
      persistentParams
    } = this.props

    this.props.fetchMethod({
      ...persistentParams,
      ...filters
    })
  }

  render () {
    const {
      columnGroups
    } = this.props

    return (
      <TableHead>
        <TableRow>
          {columnGroups.map(this.renderColumnGroup)}
        </TableRow>
        <SearchTableRow>
          {columnGroups.map(this.renderHeaderFilters)}
        </SearchTableRow>
      </TableHead>
    )
  }
}

BaseTableHeader.defaultProps = {
  columnGroups: [],
  sortBy: Object.empty
}

BaseTableHeader.propTypes = {
  columnGroups: PropTypes.array,
  changeSort: PropTypes.func,
  filterItems: PropTypes.func,
  fetchMethod: PropTypes.func,
  sortBy: PropTypes.object,
  table: PropTypes.string,
  dispatchableMethods: dispatchableMethodsPropType,
  filters: PropTypes.object,
  persistentParams: PropTypes.object,
  isLoading: PropTypes.bool
}

const mapStateToProps = (state, props) => {
  const tableData = Object.tryGetProperty(
    state,
    ({ table }) => table[props.table],
    Object.empty
  ) || Object.empty

  return {
    filters: tableData.filters || Object.empty,
    persistentParams: tableData.persistentParams || Object.empty,
    isLoading: state.common.isLoading
  }
}

export default connect(mapStateToProps, {
  filterItems
})(BaseTableHeader)
