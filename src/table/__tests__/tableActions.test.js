import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import {
  tableInitAction,
  tableDestroyAction,
  changeTableParamsAction,
  selectItemAction,
  deselectItemAction,
  deselectAllItemsAction,
  tableFilterAction
} from '../../redux/actions/tables/ActionCreators'

import {
  initTable,
  destroyTable,
  changeTableParams,
  selectItem,
  deselectItem,
  deselectAllItems,
  filterItems
} from '../../redux/actions/tables/Actions'

import {
  tableInitParams
} from './mock'

const mockStore = configureStore([ thunk ])

describe('Table actions', () => {
  let store

  beforeEach(() => {
    store = mockStore({})
  })

  const tableId = 'TABLE_TEST'

  it('INIT TABLE', () => {
    store.dispatch(initTable(tableId, tableInitParams))
    store.dispatch(initTable())

    expect(store.getActions()).toContainEqual(tableInitAction(tableId, tableInitParams))
    expect(store.getActions()).toContainEqual(tableInitAction(undefined, {}))
  })

  it('DESTROY TABLE', () => {
    store.dispatch(destroyTable(tableId))
    store.dispatch(destroyTable())
    expect(store.getActions()).toContainEqual(tableDestroyAction(tableId))
    expect(store.getActions()).toContainEqual(tableDestroyAction())
  })

  it('CHANGE PARAMS', () => {
    store.dispatch(changeTableParams(tableId, { page: 2 }))
    store.dispatch(changeTableParams(tableId, { limit: 25 }))
    store.dispatch(changeTableParams(tableId, { limit: 25, page: 1 }))
    expect(store.getActions()).toContainEqual(changeTableParamsAction(tableId, { page: 2 }))
    expect(store.getActions()).toContainEqual(changeTableParamsAction(tableId, { limit: 25 }))
    expect(store.getActions()).toContainEqual(changeTableParamsAction(tableId, { limit: 25, page: 1 }))
  })

  it('SELECT ITEM', () => {
    store.dispatch(selectItem(tableId))
    store.dispatch(selectItem(tableId, 2))
    store.dispatch(selectItem(tableId, 'item'))

    expect(store.getActions()).toContainEqual(selectItemAction(tableId, undefined))
    expect(store.getActions()).toContainEqual(selectItemAction(tableId, 2))
    expect(store.getActions()).toContainEqual(selectItemAction(tableId, 'item'))
  })

  it('DESELECT ITEM', () => {
    store.dispatch(deselectItem(tableId))
    store.dispatch(deselectItem(tableId, 2))
    store.dispatch(deselectItem(tableId, 'item'))

    expect(store.getActions()).toContainEqual(deselectItemAction(tableId, undefined))
    expect(store.getActions()).toContainEqual(deselectItemAction(tableId, 2))
    expect(store.getActions()).toContainEqual(deselectItemAction(tableId, 'item'))
  })

  it('DESELECT ALL ITEMS', () => {
    store.dispatch(deselectAllItems(tableId))
    store.dispatch(deselectAllItems())

    expect(store.getActions()).toContainEqual(deselectAllItemsAction(tableId))
    expect(store.getActions()).toContainEqual(deselectAllItemsAction(undefined))
  })

  it('FILTER ITEMS', () => {
    store.dispatch(filterItems(tableId))
    store.dispatch(filterItems())

    expect(store.getActions()).toContainEqual(tableFilterAction(tableId))
    expect(store.getActions()).toContainEqual(tableFilterAction(undefined))
  })
})
