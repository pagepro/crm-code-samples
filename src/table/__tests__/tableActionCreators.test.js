import * as actionTypes from '../../redux/types/table'

import {
  tableInitAction,
  tableDestroyAction,
  changeTableParamsAction,
  selectItemAction,
  deselectItemAction,
  deselectAllItemsAction,
  tableFilterAction
} from '../../redux/actions/tables/ActionCreators'

describe('Table action creators', () => {
  const tableId = 'TABLE_TEST'

  describe('should init table', () => {
    it('without params', () => {
      expect(tableInitAction(tableId)).toEqual({
        type: actionTypes.TABLE_INIT,
        payload: {
          id: tableId
        }
      })
    })
    it('with params', () => {
      expect(tableInitAction(tableId, { limit: 10, page: 1 })).toEqual({
        type: actionTypes.TABLE_INIT,
        payload: {
          id: tableId,
          params: {
            limit: 10,
            page: 1
          }
        }
      })
    })
  })

  it('should destroy table', () => {
    expect(tableDestroyAction(tableId)).toEqual({
      type: actionTypes.TABLE_DESTROY,
      payload: tableId
    })

    expect(tableDestroyAction(null)).toEqual({
      type: actionTypes.TABLE_DESTROY,
      payload: null
    })
  })

  it('should update table params', () => {
    expect(changeTableParamsAction(tableId, { page: 2 })).toEqual({
      type: actionTypes.TABLE_UPDATE_PARAMS,
      payload: {
        id: tableId,
        params: {
          page: 2
        }
      }
    })

    expect(changeTableParamsAction(tableId)).toEqual({
      type: actionTypes.TABLE_UPDATE_PARAMS,
      payload: {
        id: tableId,
        params: undefined
      }
    })

    expect(changeTableParamsAction(tableId, {})).toEqual({
      type: actionTypes.TABLE_UPDATE_PARAMS,
      payload: {
        id: tableId,
        params: {}
      }
    })
  })

  it('should select items', () => {
    expect(selectItemAction(tableId, 2)).toEqual({
      type: actionTypes.TABLE_SELECT_ITEM,
      payload: {
        id: tableId,
        item: 2
      }
    })

    expect(selectItemAction(tableId)).toEqual({
      type: actionTypes.TABLE_SELECT_ITEM,
      payload: {
        id: tableId,
        item: undefined
      }
    })
  })

  it('should deselect items', () => {
    expect(deselectItemAction(tableId, 1)).toEqual({
      type: actionTypes.TABLE_DESELECT_ITEM,
      payload: {
        id: tableId,
        item: 1
      }
    })

    expect(deselectItemAction(tableId)).toEqual({
      type: actionTypes.TABLE_DESELECT_ITEM,
      payload: {
        id: tableId,
        item: undefined
      }
    })
  })

  it('should deselect all items', () => {
    expect(deselectAllItemsAction(tableId)).toEqual({
      type: actionTypes.TABLE_DESELECT_ALL_ITEMS,
      payload: tableId
    })

    expect(deselectAllItemsAction()).toEqual({
      type: actionTypes.TABLE_DESELECT_ALL_ITEMS,
      payload: undefined
    })
  })

  it('should filter items', () => {
    const data = {
      filters: {
        test: 'test'
      },
      autocomplete: {
        test: 'test'
      }
    }

    expect(tableFilterAction(tableId, data)).toEqual({
      type: actionTypes.TABLE_FILTER,
      payload: {
        id: tableId,
        ...data
      }
    })

    expect(tableFilterAction(tableId)).toEqual({
      type: actionTypes.TABLE_FILTER,
      payload: {
        id: tableId,
        filters: {},
        autocomplete: {}
      }
    })

    expect(tableFilterAction()).toEqual({
      type: actionTypes.TABLE_FILTER,
      payload: {
        id: undefined,
        filters: {},
        autocomplete: {}
      }
    })
  })
})
