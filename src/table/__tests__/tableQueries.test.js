import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import tableQueries, { TableQueries } from '../tableQueries'
import store from '../../redux'

describe('TableQueries component tests', () => {
  const fetchMethod = () => Promise.resolve()
  const filterItems = () => Promise.resolve()
  const component = () => <div />
  const defaultTableQueries = (
    <TableQueries
      autocomplete={{}}
      filters={{}}
      filtersConfig={[]}
      fetchMethod={fetchMethod}
      filterItems={filterItems}
      component={component}
    />
  )

  it('should render', async () => {
    const wrapper = shallow(defaultTableQueries)
    expect(wrapper.find(component)).toHaveLength(1)
  })

  it('should enchance component with default props', async () => {
    const wrapper = shallow(defaultTableQueries)
    const props = await wrapper.find(component).props()
    expect(Object.keys(props)).toHaveLength(7)
  })

  it('should enchance component with given props', async () => {
    const props = {
      autocomplete: {
        test: 'test'
      },
      filters: {
        test: 'test'
      },
      filtersConfig: [
        {
          name: 'user',
          placeholder: 'Find user'
        }
      ]
    }

    const wrapper = await shallow(defaultTableQueries).setProps(props)
    const receivedProps = await wrapper.find(component).props()
    expect(receivedProps).toMatchObject(props)
  })

  it('filterData should invoke filterItems actions and fetchMethod with given params', async () => {
    const filterItems = sinon.fake.resolves()
    const defaultArgs = {
      autocomplete: {
        test: 'test'
      },
      filters: {
        test: 'test'
      }
    }
    const newArgs = {
      autocomplete: {
        test2: 'test2'
      },
      filters: {
        test2: 'test2'
      }
    }
    const classFetchMethod = sinon.fake.resolves()

    const wrapper = shallow(defaultTableQueries).setProps({
      filterItems
    })

    sinon.replace(wrapper.instance(), 'fetchMethod', classFetchMethod)
    // first call
    await wrapper.instance().filterData(defaultArgs.filters, defaultArgs.autocomplete)
    expect(classFetchMethod.calledWith(defaultArgs.filters))
    expect(filterItems.calledWith(defaultArgs))
    // second call
    await wrapper.instance().filterData(newArgs)
    expect(classFetchMethod.calledWith(newArgs.filters))
    expect(classFetchMethod.calledTwice)
    expect(filterItems.calledWith(newArgs))
  })

  it('should debounce invoke fetchMethod', async () => {
    const fetchMethod = sinon.fake.resolves()
    const wrapper = shallow(defaultTableQueries).setProps({
      fetchMethod
    })

    await wrapper.instance().fetchMethod()
    expect(fetchMethod.notCalled)
    wrapper.instance().fetchMethod.flush()
    expect(fetchMethod.called)
  })

  it('should get data from event onChange and invoke filterData method', async () => {
    const classFilterData = sinon.fake.resolves()
    const name = 'test'
    const value = 'test'
    const event = {
      preventDefault: sinon.fake(),
      target: {
        name,
        value
      }
    }

    const wrapper = shallow(defaultTableQueries).setProps({
      filterItems
    })

    sinon.replace(wrapper.instance(), 'filterData', classFilterData)

    await wrapper.instance().onChange(event)
    expect(classFilterData.called)
    expect(event.preventDefault.called)
    expect(classFilterData.calledWith({[name]: value}))
  })
  it('should get data from autocomplete handler and invoke filterData method', async () => {
    const classFilterData = sinon.fake.resolves()
    const defaultArgs = {
      autocomplete: {
        test: 'test'
      },
      filters: {
        test: 'test'
      }
    }

    const wrapper = shallow(defaultTableQueries).setProps({
      filterItems
    })

    sinon.replace(wrapper.instance(), 'filterData', classFilterData)

    await wrapper.instance().onAutocomplete(defaultArgs.filters, defaultArgs.autocomplete)
    expect(classFilterData.called)
    expect(classFilterData.calledWith(defaultArgs.filters, defaultArgs.autocomplete))
  })
})

describe('tableQueries HOC tests', async () => {
  const fetchMethod = () => Promise.resolve()
  const table = 'test'
  const component = () => <div />
  const DefaultTableQueries = tableQueries({
    fetchMethod,
    table
  })(component)

  it('should render', async () => {
    const test = shallow(<DefaultTableQueries store={store} />)
    const component = test.dive()
    expect(component.find('TableQueries')).toHaveLength(1)
  })

  it('should pass props', async () => {
    const test = shallow(<DefaultTableQueries store={store} />)
    const component = test.dive()
    const componentProps = component.props()
    expect(componentProps.table).toBe(table)
    expect(componentProps.filtersConfig).toEqual([])
    expect(componentProps.filters).toEqual({})
    expect(componentProps.autocomplete).toEqual({})
  })
})
