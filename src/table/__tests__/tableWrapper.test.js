import React from 'react'
import { shallow } from 'enzyme'
import sinon from 'sinon'
import {
  changeTableParams,
  deselectAllItems,
  deselectItem,
  destroyTable,
  initTable,
  selectItem
} from '../../redux/actions/tables/Actions'
import tableWrapper, { TableWrapper } from '../tableWrapper'
import BaseTableHeader from '../BaseTableHeader'
import store from '../../redux'

const data = Array.from({length: 10}, (v, k) => ({id: k + 1}))

describe('TableWrapper component tests', () => {
  const header = [{ name: 'name', slug: 'slug', width: '50%' }]
  const fetchMethod = () => Promise.resolve()
  const component = () => <div />
  const defaultTableWrapper = (
    <TableWrapper
      fetchMethod={fetchMethod}
      initTable={initTable}
      deselectItem={deselectItem}
      deselectAllItems={deselectAllItems}
      changeTableParams={changeTableParams}
      destroyTable={destroyTable}
      selectItem={selectItem}
      component={component}
      header={header}
    />
  )

  it('should render', async () => {
    const wrapper = shallow(defaultTableWrapper)
    expect(wrapper.find(component)).toHaveLength(1)
  })

  it('should render BaseTableHeader component when header passed as an array', async () => {
    const wrapper = shallow(defaultTableWrapper)
    expect(wrapper.find(BaseTableHeader)).toHaveLength(1)
  })

  it('should enchance component with default props', async () => {
    const wrapper = shallow(defaultTableWrapper)
    const props = await wrapper.find(component).props()
    expect(Object.keys(props)).toHaveLength(2)
  })

  it('should enchance component with given props', async () => {
    const props = {
      page: 2,
      limit: 10,
      count: 20,
      data
    }

    const wrapper = await shallow(defaultTableWrapper).setProps(props)
    const receivedProps = await wrapper.find(component).props()
    expect(receivedProps.table).toMatchObject(props)
  })

  it('should update headers props', async () => {
    const props = {
      header: [
        {
          name: 'name1',
          slug: 'slug1',
          width: '50%'
        },
        {
          name: 'name2'
        }
      ]
    }

    const wrapper = await shallow(defaultTableWrapper).setProps(props)
    const receivedProps = await wrapper.find(BaseTableHeader).props()
    expect(receivedProps.columns).toMatchObject(props.header)
  })

  it('should has loading state', async () => {
    const wrapper = shallow(defaultTableWrapper)
    expect(wrapper.state('isLoading')).toBe(true)
  })

  it('toggleLoader method should update state', async () => {
    const wrapper = shallow(defaultTableWrapper)
    expect(wrapper.state('isLoading')).toBe(true)
    wrapper.instance().toggleLoader()
    expect(wrapper.state('isLoading')).toBe(false)
  })

  it('getData method should handle loader', async () => {
    const wrapper = shallow(defaultTableWrapper)
    expect(wrapper.state('isLoading')).toBe(true)
    await wrapper.instance().getData()
    expect(wrapper.state('isLoading')).toBe(false)
  })

  it('getData method should invoke fetchMethod with given params', async () => {
    const fetchMethod = sinon.fake.resolves()
    const defaultArgs = {
      page: 1,
      limit: 10
    }
    const newArgs = {
      page: 2,
      limit: 20,
      sort: 'first_name',
      direction: 'asc'
    }

    const wrapper = shallow(defaultTableWrapper).setProps({
      fetchMethod
    })
    await wrapper.instance().getData()
    expect(fetchMethod.calledOnce)
    await wrapper.instance().getData(newArgs)
    const firstArgs = fetchMethod.args[0]
    const secondArgs = fetchMethod.args[1]
    expect(fetchMethod.calledTwice)
    expect(firstArgs[0]).toEqual(defaultArgs)
    expect(secondArgs[0]).toEqual(newArgs)
  })

  it('destroyTable method should invoke destroyTable passed in props', async () => {
    const destroyTable = sinon.fake.resolves()

    const wrapper = await shallow(defaultTableWrapper).setProps({
      destroyTable
    })
    await wrapper.instance().destroyTable()
    expect(destroyTable.calledOnce)
  })

  it('should invoke destroyTable on component unmount', async () => {
    const destroyTable = sinon.fake.resolves()

    const wrapper = await shallow(defaultTableWrapper).setProps({
      destroyTable
    })
    await wrapper.unmount()
    expect(destroyTable.calledOnce)
  })
})

describe('tableWrapper HOC tests', async () => {
  const fetchMethod = () => Promise.resolve()
  const dataSelector = () => data
  const countSelector = () => data.length
  const table = 'test'
  const component = () => <div />
  const DefaultTableWrapper = tableWrapper({
    fetchMethod,
    dataSelector,
    countSelector,
    table
  })(component)

  it('should render', async () => {
    const test = shallow(<DefaultTableWrapper store={store} />)
    const component = test.dive()
    expect(component.find('TableWrapper')).toHaveLength(1)
  })

  it('should pass props', async () => {
    const test = shallow(<DefaultTableWrapper store={store} />)
    const component = test.dive()
    const componentProps = component.props()
    expect(componentProps.table).toBe(table)
    expect(componentProps.data).toEqual(data)
    expect(componentProps.page).toBe(1)
    expect(componentProps.count).toBe(10)
    expect(componentProps.sortBy).toEqual({})
    expect(componentProps.limit).toBe(10)
  })
})
