import tableReducer, {
  initialState
} from '../../redux/reducers/table'
import {
  tableInitAction,
  tableDestroyAction,
  changeTableParamsAction,
  selectItemAction,
  deselectItemAction,
  deselectAllItemsAction
} from '../../redux/actions/tables/ActionCreators'

import { tableInitParams } from './mock'

describe('TABLE REDUCER', () => {
  const tableId = 'TABLE_TEST'

  const withTable = tableReducer(undefined, tableInitAction(tableId))

  it('should be equal to initals state at the beggining', () => {
    const result = tableReducer(undefined, {})
    expect(result).toEqual(initialState)
  })

  it('init table', () => {
    const result = tableReducer(undefined, tableInitAction())
    const resultWithParams = tableReducer(undefined, tableInitAction(tableId, tableInitParams))

    expect(withTable).toEqual({
      [tableId]: {
        ...tableInitParams,
        id: tableId
      }
    })
    expect(result).toEqual(initialState)
    expect(resultWithParams).toEqual({
      [tableId]: {
        ...tableInitParams,
        id: tableId
      }
    })
  })

  it('destroy table', () => {
    const result = tableReducer(withTable, tableDestroyAction(tableId))
    const resultNoId = tableReducer(withTable, tableDestroyAction())
    const resultInvalidId = tableReducer(withTable, tableDestroyAction('asd'))
    expect(result).toEqual(initialState)
    expect(resultNoId).toEqual(withTable)
    expect(resultInvalidId).toEqual(withTable)
  })

  it('change table params', () => {
    const resultLimit = tableReducer(withTable, changeTableParamsAction(tableId, { limit: 100 }))
    const resultPage = tableReducer(withTable, changeTableParamsAction(tableId, { page: 3 }))
    const resultMix = tableReducer(withTable, changeTableParamsAction(tableId, { limit: 50, page: 2 }))
    const resultInvalidId = tableReducer(withTable, changeTableParamsAction('asdads', { limit: 50, page: 3 }))
    const resultNoParams = tableReducer(withTable, changeTableParamsAction(tableId))

    expect(resultLimit).toEqual({
      [tableId]: {
        ...tableInitParams,
        id: tableId,
        limit: 100,
        page: 1
      }
    })
    expect(resultPage).toEqual({
      [tableId]: {
        ...tableInitParams,
        id: tableId,
        limit: 10,
        page: 3
      }
    })
    expect(resultMix).toEqual({
      [tableId]: {
        ...tableInitParams,
        id: tableId,
        limit: 50,
        page: 2
      }
    })
    expect(resultInvalidId).toEqual(withTable)
    expect(resultNoParams).toEqual(withTable)
  })

  it('select items from table', () => {
    const resultCorrectNumber = tableReducer(withTable, selectItemAction(tableId, 2))
    const resultCorrectString = tableReducer(withTable, selectItemAction(tableId, 'asd'))
    const resultInvalidType = tableReducer(withTable, selectItemAction(tableId, { id: 1 }))
    const resultNoValue = tableReducer(withTable, selectItemAction(tableId))
    const resultInvalidId = tableReducer(withTable, selectItemAction('as', 1))

    expect(resultCorrectNumber).toEqual({
      [tableId]: {
        ...tableInitParams,
        id: tableId,
        selected: {
          '2': true
        }
      }
    })
    expect(resultCorrectString).toEqual({
      [tableId]: {
        ...tableInitParams,
        id: tableId,
        selected: {
          asd: true
        }
      }
    })
    expect(resultInvalidType).toEqual(withTable)
    expect(resultNoValue).toEqual(withTable)
    expect(resultInvalidId).toEqual(withTable)
  })

  it('deselect items from table', () => {
    const resultWithSelectedItem = tableReducer(withTable, selectItemAction(tableId, 1))
    const resultDeselectedSelected = tableReducer(resultWithSelectedItem, deselectItemAction(tableId, 1))
    const resultCorrectNumber = tableReducer(resultWithSelectedItem, deselectItemAction(tableId, 2))
    const resultCorrectString = tableReducer(resultWithSelectedItem, deselectItemAction(tableId, 'asd'))
    const resultInvalidType = tableReducer(withTable, deselectItemAction(tableId, { id: 1 }))
    const resultNoValue = tableReducer(withTable, deselectItemAction(tableId))
    const resultInvalidId = tableReducer(withTable, deselectItemAction('as', 1))

    expect(resultDeselectedSelected).toEqual({
      [tableId]: {
        ...tableInitParams,
        id: tableId,
        selected: {}
      }
    })
    expect(resultCorrectNumber).toEqual({
      [tableId]: {
        ...tableInitParams,
        id: tableId,
        selected: {
          '1': true
        }
      }
    })
    expect(resultCorrectString).toEqual({
      [tableId]: {
        ...tableInitParams,
        id: tableId,
        selected: {
          '1': true
        }
      }
    })
    expect(resultInvalidType).toEqual(withTable)
    expect(resultNoValue).toEqual(withTable)
    expect(resultInvalidId).toEqual(withTable)
  })

  it('deselect all items from table', () => {
    const resultWithSelectedItem = tableReducer(withTable, selectItemAction(tableId, 1))
    const resultWithMultipleSelectedItems = tableReducer(resultWithSelectedItem, selectItemAction(tableId, 2))
    const resultCorrect = tableReducer(resultWithMultipleSelectedItems, deselectAllItemsAction(tableId))
    const resultNoValue = tableReducer(resultWithMultipleSelectedItems, deselectAllItemsAction())
    const resultInvalidId = tableReducer(resultWithMultipleSelectedItems, deselectAllItemsAction('as'))

    expect(resultCorrect).toEqual({
      [tableId]: {
        ...tableInitParams,
        id: tableId
      }
    })
    expect(resultNoValue).toEqual(resultWithMultipleSelectedItems)
    expect(resultNoValue).toEqual(resultWithMultipleSelectedItems)
    expect(resultInvalidId).toEqual(resultWithMultipleSelectedItems)
  })
})
