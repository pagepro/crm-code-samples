import React from 'react'
import PropTypes from 'prop-types'
import { TableHeaderWithArrows } from '../components/table/TableHeader'
import TableRowHeader from '../components/table/TableRowHeader'

export const ORDER = {
  ASCENDING: 'asc',
  DESCENDING: 'desc'
}
Object.freeze(ORDER)

class BaseTableHeader extends React.Component {
  constructor (props) {
    super(props)

    this.renderColumn = this.renderColumn.bind(this)
    this.sortBy = this.sortBy.bind(this)
  }

  sortBy (slug) {
    return () => {
      if (!slug) return
      const {
        changeSort,
        sortBy: {
          sort,
          direction
        }
      } = this.props

      changeSort({
        sort: slug,
        direction: direction === ORDER.DESCENDING || sort !== slug
          ? ORDER.ASCENDING
          : ORDER.DESCENDING
      })
    }
  }

  renderColumn (item) {
    const {
      name,
      slug,
      width
    } = item

    const {
      sortBy: {
        direction,
        sort
      }
    } = this.props

    const now = new Date()

    return (
      <TableHeaderWithArrows
        fade
        active={slug && slug === sort}
        clickable={!!slug}
        direction={direction}
        key={`${name}-${now.getTime()}`}
        onClick={this.sortBy(slug)}
        width={width}
      >
        {name}
      </TableHeaderWithArrows>
    )
  }

  render () {
    const {
      columns
    } = this.props

    return (
      <TableRowHeader>
        {columns.map(this.renderColumn)}
      </TableRowHeader>
    )
  }
}

BaseTableHeader.defaultProps = {
  columns: [],
  sortBy: {}
}

BaseTableHeader.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    slug: PropTypes.string,
    width: PropTypes.string
  })),
  changeSort: PropTypes.func,
  sortBy: PropTypes.object
}

export default BaseTableHeader
