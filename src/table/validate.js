export default (functionFields, stringFields) => {
  [
    ...functionFields,
    ...stringFields
  ].forEach(field => {
    if (!field) console.warn(`${field} param is required!`)
  })
  functionFields.forEach(field => {
    if (typeof field !== 'function') console.warn(`${field} must be a function!`)
  })
  stringFields.forEach(field => {
    if (typeof field !== 'string') console.warn(`${field} must be a string!`)
  })
}
